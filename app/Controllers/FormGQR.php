<?php

namespace App\Controllers;

use App\Models\TData;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class FormGQR extends BaseController
{
    protected $tData;
    public function __construct()
    {
        $this->tData = new TData();
    }

    public function index()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tData->getData(),
        ];
        return view('pages/forn/formInputQR', $data);
    }

    public function fetch()
    {
        $areaModel = model(tData::class);
        $areaResult = $areaModel->findAll();

        return $this->response->setJSON($areaResult);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tData->getData($id),
        ];
        return view('pages/detail2', $data);
    }

    public function json($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tData->getData($id),
        ];
        return $this->response->setJSON($data);
    }
    public function qr($id)
    {

        $writer = new PngWriter();

        // Create QR code
        $qrCode = QrCode::create('http://localhost:8080/adminqr/json/'.$id)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        $result->saveToFile('assets/images/qrcode/item-'.$id.'.png');
        return redirect()->to('/adminqr/create');
    }

    public function update($id)
    {
        $this->tData->save([
            'id' => $id,
            'no_rfq' => $this->request->getVar('no_rfq'),
            'no_wo' => $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust'),
            'code_qr' => $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Diubah');
        return redirect()->to('/adminqr/create');
    }

    public function create()
    {
        $data = [
            'title' => 'Form Input Item',
            'item' => $this->tData->getData(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/forn/formInputQR', $data);
    }

    public function save()
    {
        $this->tData->save([
            'no_rfq' => $this->request->getVar('no_rfq'),
            'no_wo' => $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust'),
            'code_qr' => $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Ditambahkan');
        return redirect()->to('/adminqr/create');
    }
}
