<?php

namespace App\Controllers;

use App\Models\TItems;
use App\Models\TOut;
use App\Models\TReceh;

class Home extends BaseController
{
    protected $tItems;
    protected $tOut;
    protected $tReceh;
    public function __construct()
    {
        $this->tItems = new TItems();
        $this->tOut = new TOut();
        $this->tReceh = new TReceh();
    }

    private function countRackA()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $A = "A";
        $QTYRecehA = [];
        $uniqueRack = [];

        $locOnA1 = [];
        $countOnA1 = count($locOnA1);

        $locOnA2 = [];
        $countOnA2 = count($locOnA2);

        $locOnA3 = [];
        $countOnA3 = count($locOnA3);

        $locOnA4 = [];
        $countOnA4 = count($locOnA4);

        $locOnA5 = [];
        $countOnA5 = count($locOnA5);

        $locOffA1 = [];
        $countOffA1 = count($locOffA1);

        $locOffA2 = [];
        $countOffA2 = count($locOffA2);

        $locOffA3 = [];
        $countOffA3 = count($locOffA3);

        $locOffA4 = [];
        $countOffA4 = count($locOffA4);

        $locOffA5 = [];
        $countOff5 = count($locOffA5);

        $QTYA = [];

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $A) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehA[]  = $qty;

                    $countQTYRecehA = 0;
                    for ($i = 0; $i < count($QTYRecehA); $i++) {
                        $countQTYRecehA += $QTYRecehA[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehA)) {
            $countQTYRecehA = 0;
        }

        foreach ($item['item'] as $v) {
            if ($v['rack'] === $A) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnA1)) {
                            $locOnA1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnA1 = count($locOnA1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffA1)) {
                            $locOffA1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffA1 = count($locOffA1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnA2)) {
                            $locOnA2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnA2 = count($locOnA2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffA2)) {
                            $locOffA2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffA2 = count($locOffA2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnA3)) {
                            $locOnA3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnA3 = count($locOnA3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffA3)) {
                            $locOffA3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffA3 = count($locOffA3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnA4)) {
                            $locOnA4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnA4 = count($locOnA4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffA4)) {
                            $locOffA4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffA4 = count($locOffA4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnA5)) {
                            $locOnA5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnA5 = count($locOnA5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffA5)) {
                            $locOffA5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOffA5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTYA[]  = $qty;

                    $countQTYA = 0;
                    for ($i = 0; $i < count($QTYA); $i++) {
                        $countQTYA += $QTYA[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYA)) {
            $countQTYA = 0;
        }
        if (empty($locOnA1)) {
            $countOnA1 = 0;
        }
        if (empty($locOffA1)) {
            $countOffA1 = 0;
        }
        if (empty($locOnA2)) {
            $countOnA2 = 0;
        }
        if (empty($locOffA2)) {
            $countOffA2 = 0;
        }
        if (empty($locOnA3)) {
            $countOnA3 = 0;
        }
        if (empty($locOffA3)) {
            $countOffA3 = 0;
        }
        if (empty($locOnA4)) {
            $countOnA4 = 0;
        }
        if (empty($locOffA4)) {
            $countOffA4 = 0;
        }
        if (empty($locOnA5)) {
            $countOnA5 = 0;
        }
        if (empty($locOffA5)) {
            $countOff5 = 0;
        }

        $totalCountOffA = ($countOffA1 + $countOffA2 + $countOffA3 + $countOffA4 + $countOff5);
        $totalCountOnA = ($countOnA1 + $countOnA2 + $countOnA3 + $countOnA4 + $countOnA5);

        $rackA = [
            'totalOnA' => $totalCountOnA,
            'totalOffA' => $totalCountOffA,
            'totalQTYA' => ($countQTYA + $countQTYRecehA),
        ];

        return $rackA;
    }

    private function countRackB()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehB = [];
        $uniqueRack = [];
        $B = "B";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $B) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehB[]  = $qty;

                    $countQTYRecehB = 0;
                    for ($i = 0; $i < count($QTYRecehB); $i++) {
                        $countQTYRecehB += $QTYRecehB[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehB)) {
            $countQTYRecehB = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $B) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYB = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYB += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYB = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffB = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnB = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackB = [
            'totalOnB' => $totalCountOnB,
            'totalOffB' => $totalCountOffB,
            'totalQTYB' => ($countQTYB + $countQTYRecehB),
        ];

        return $rackB;
    }
    private function countRackC()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $uniqueRack = [];
        $QTYRecehC = [];
        $C = "C";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $C) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehC[]  = $qty;

                    $countQTYRecehX = 0;
                    for ($i = 0; $i < count($QTYRecehC); $i++) {
                        $countQTYRecehX += $QTYRecehC[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehC)) {
            $countQTYRecehC = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $C) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYC = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYC += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYC = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffC = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnC = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackC = [
            'totalOnC' => $totalCountOnC,
            'totalOffC' => $totalCountOffC,
            'totalQTYC' => ($countQTYC + $countQTYRecehC),
        ];

        return $rackC;
    }

    private function countRackD()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehD = [];
        $D = "D";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $D) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehD[]  = $qty;

                    $countQTYRecehD = 0;
                    for ($i = 0; $i < count($QTYRecehD); $i++) {
                        $countQTYRecehD += $QTYRecehD[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehD)) {
            $countQTYRecehD = 0;
        }

        $uniqueRack = [];
        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $D) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYD = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYD += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan

            }
        }
        if (empty($QTY)) {
            $countQTYD = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffD = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnD = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackD = [
            'totalOnD' => $totalCountOnD,
            'totalOffD' => $totalCountOffD,
            'totalQTYD' => ($countQTYD + $countQTYRecehD),
        ];

        return $rackD;
    }

    private function countRackE()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $E = "E";
        $QTYRecehE = [];
        $uniqueRack = [];

        $locOnE1 = [];
        $countOnE1 = count($locOnE1);

        $locOnE2 = [];
        $countOnE2 = count($locOnE2);

        $locOnE3 = [];
        $countOnE3 = count($locOnE3);

        $locOnE4 = [];
        $countOnE4 = count($locOnE4);

        $locOnE5 = [];
        $countOnE5 = count($locOnE5);

        $locOffE1 = [];
        $countOffE1 = count($locOffE1);

        $locOffE2 = [];
        $countOffE2 = count($locOffE2);

        $locOffE3 = [];
        $countOffE3 = count($locOffE3);

        $locOffE4 = [];
        $countOffE4 = count($locOffE4);

        $locOffE5 = [];
        $countOffE5 = count($locOffE5);

        $QTYE = [];

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $E) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehE[]  = $qty;

                    $countQTYRecehE = 0;
                    for ($i = 0; $i < count($QTYRecehE); $i++) {
                        $countQTYRecehE += $QTYRecehE[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehE)) {
            $countQTYRecehE = 0;
        }

        foreach ($item['item'] as $v) {
            if ($v['rack'] === $E) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnE1)) {
                            $locOnE1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnE1 = count($locOnE1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffE1)) {
                            $locOffE1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffE1 = count($locOffE1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnE2)) {
                            $locOnE2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnE2 = count($locOnE2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffE2)) {
                            $locOffE2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffE2 = count($locOffE2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnE3)) {
                            $locOnE3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnE3 = count($locOnE3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffE3)) {
                            $locOffE3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffE3 = count($locOffE3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnE4)) {
                            $locOnE4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnE4 = count($locOnE4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffE4)) {
                            $locOffE4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffE4 = count($locOffE4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOnE5)) {
                            $locOnE5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOnE5 = count($locOnE5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOffE5)) {
                            $locOffE5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOffE5 = count($locOffE5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTYE[]  = $qty;

                    $countQTYE = 0;
                    for ($i = 0; $i < count($QTYE); $i++) {
                        $countQTYE += $QTYE[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYE)) {
            $countQTYE = 0;
        }
        if (empty($locOnE1)) {
            $countOnE1 = 0;
        }
        if (empty($locOffE1)) {
            $countOffE1 = 0;
        }
        if (empty($locOnE2)) {
            $countOnE2 = 0;
        }
        if (empty($locOffE2)) {
            $countOffE2 = 0;
        }
        if (empty($locOnE3)) {
            $countOnE3 = 0;
        }
        if (empty($locOffE3)) {
            $countOffE3 = 0;
        }
        if (empty($locOnE4)) {
            $countOnE4 = 0;
        }
        if (empty($locOffE4)) {
            $countOffE4 = 0;
        }
        if (empty($locOnE5)) {
            $countOnE5 = 0;
        }
        if (empty($locOffE5)) {
            $countOffE5 = 0;
        }

        $totalCountOffE = ($countOffE1 + $countOffE2 + $countOffE3 + $countOffE4 + $countOffE5);
        $totalCountOnE = ($countOnE1 + $countOnE2 + $countOnE3 + $countOnE4 + $countOnE5);

        $rackE = [
            'totalOnE' => $totalCountOnE,
            'totalOffE' => $totalCountOffE,
            'totalQTYE' => ($countQTYE + $countQTYRecehE),
        ];

        return $rackE;
    }

    private function countRackF()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehF = [];
        $uniqueRack = [];
        $F = "F";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $F) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehF[]  = $qty;

                    $countQTYRecehF = 0;
                    for ($i = 0; $i < count($QTYRecehF); $i++) {
                        $countQTYRecehF += $QTYRecehF[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehF)) {
            $countQTYRecehF = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $F) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYF = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYF += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYF = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffF = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnF = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackF = [
            'totalOnF' => $totalCountOnF,
            'totalOffF' => $totalCountOffF,
            'totalQTYF' => ($countQTYF + $countQTYRecehF),
        ];

        return $rackF;
    }
    private function countRackH()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $uniqueRack = [];
        $QTYRecehH = [];
        $H = "H";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $H) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehH[]  = $qty;

                    $countQTYRecehH = 0;
                    for ($i = 0; $i < count($QTYRecehH); $i++) {
                        $countQTYRecehH += $QTYRecehH[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehH)) {
            $countQTYRecehH = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $H) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYH = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYH += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYH = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffH = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnH = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackH = [
            'totalOnH' => $totalCountOnH,
            'totalOffH' => $totalCountOffH,
            'totalQTYH' => ($countQTYH + $countQTYRecehH),
        ];

        return $rackH;
    }
    private function countRackG()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehG = [];
        $G = "G";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $G) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehG[]  = $qty;

                    $countQTYRecehG = 0;
                    for ($i = 0; $i < count($QTYRecehG); $i++) {
                        $countQTYRecehG += $QTYRecehG[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehG)) {
            $countQTYRecehG = 0;
        }

        $uniqueRack = [];
        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $G) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYG = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYG += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan

            }
        }
        if (empty($QTY)) {
            $countQTYG = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffG = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnG = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackG = [
            'totalOnG' => $totalCountOnG,
            'totalOffG' => $totalCountOffG,
            'totalQTYG' => ($countQTYG + $countQTYRecehG),
        ];

        return $rackG;
    }

    private function countRackI()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehI = [];
        $uniqueRack = [];
        $I = "I";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $I) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehI[]  = $qty;

                    $countQTYRecehI = 0;
                    for ($i = 0; $i < count($QTYRecehI); $i++) {
                        $countQTYRecehI += $QTYRecehI[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehI)) {
            $countQTYRecehI = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $I) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYI = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYI += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYI = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffI = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnI = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackI = [
            'totalOnI' => $totalCountOnI,
            'totalOffI' => $totalCountOffI,
            'totalQTYI' => ($countQTYI + $countQTYRecehI),
        ];

        return $rackI;
    }
    private function countRackJ()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $uniqueRack = [];
        $QTYRecehJ = [];
        $J = "J";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $J) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehJ[]  = $qty;

                    $countQTYRecehJ = 0;
                    for ($i = 0; $i < count($QTYRecehJ); $i++) {
                        $countQTYRecehJ += $QTYRecehJ[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehJ)) {
            $countQTYRecehJ = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $J) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYJ = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYJ += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYJ = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffJ = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnJ = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackJ = [
            'totalOnJ' => $totalCountOnJ,
            'totalOffJ' => $totalCountOffJ,
            'totalQTYJ' => ($countQTYJ + $countQTYRecehJ),
        ];

        return $rackJ;
    }
    private function countRackK()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehK = [];
        $K = "K";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $K) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehK[]  = $qty;

                    $countQTYRecehK = 0;
                    for ($i = 0; $i < count($QTYRecehK); $i++) {
                        $countQTYRecehK += $QTYRecehK[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehK)) {
            $countQTYRecehK = 0;
        }

        $uniqueRack = [];
        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $K) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYK = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYK += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan

            }
        }
        if (empty($QTY)) {
            $countQTYK = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffK = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnK = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackK = [
            'totalOnK' => $totalCountOnK,
            'totalOffK' => $totalCountOffK,
            'totalQTYK' => ($countQTYK + $countQTYRecehK),
        ];

        return $rackK;
    }

    private function countRackL()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehL = [];
        $uniqueRack = [];
        $L = "L";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $L) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehL[]  = $qty;

                    $countQTYRecehL = 0;
                    for ($i = 0; $i < count($QTYRecehL); $i++) {
                        $countQTYRecehL += $QTYRecehL[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehL)) {
            $countQTYRecehL = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $L) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYL = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYL += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYL = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffL = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnL = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackL = [
            'totalOnL' => $totalCountOnL,
            'totalOffL' => $totalCountOffL,
            'totalQTYL' => ($countQTYL + $countQTYRecehL),
        ];

        return $rackL;
    }
    private function countRackM()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $uniqueRack = [];
        $QTYRecehM = [];
        $M = "M";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $M) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehM[]  = $qty;

                    $countQTYRecehM = 0;
                    for ($i = 0; $i < count($QTYRecehM); $i++) {
                        $countQTYRecehM += $QTYRecehM[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehM)) {
            $countQTYRecehM = 0;
        }

        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $M) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYM = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYM += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTY)) {
            $countQTYM = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffM = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5);
        $totalCountOnM = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackM = [
            'totalOnM' => $totalCountOnM,
            'totalOffM' => $totalCountOffM,
            'totalQTYM' => ($countQTYM + $countQTYRecehM),
        ];

        return $rackM;
    }
    private function countRackN()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehN = [];
        $N = "N";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $N) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehN[]  = $qty;

                    $countQTYRecehN = 0;
                    for ($i = 0; $i < count($QTYRecehN); $i++) {
                        $countQTYRecehN += $QTYRecehN[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehN)) {
            $countQTYRecehN = 0;
        }

        $uniqueRack = [];
        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $N) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . "5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . "1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYN = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYN += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan

            }
        }
        if (empty($QTY)) {
            $countQTYN = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        $totalCountOffN = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5 - 1);
        $totalCountOnN = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5);

        $rackN = [
            'totalOnN' => $totalCountOnN,
            'totalOffN' => $totalCountOffN,
            'totalQTYN' => ($countQTYN + $countQTYRecehN),
        ];

        return $rackN;
    }

    private function countRackOthers()
    {
        $item = ['item' => $this->tItems->getItem()];
        $itemReceh = ['item' => $this->tReceh->getItem()];
        $QTYRecehOthers = [];
        $Others = "Others";
        foreach ($itemReceh['item'] as $v) {
            if ($v['rack_receh'] === $Others) {
                $qty = $v['qty_receh'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

                if ($v['id_receh'] != NULL) {

                    $QTYRecehOthers[]  = $qty;

                    $countQTYRecehOthers = 0;
                    for ($i = 0; $i < count($QTYRecehOthers); $i++) {
                        $countQTYRecehOthers += $QTYRecehOthers[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan
            }
        }
        if (empty($QTYRecehOthers)) {
            $countQTYRecehOthers = 0;
        }

        $uniqueRack = [];
        foreach ($item['item'] as $v) {
            $rack = $v['rack'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($rack, $uniqueRack)) {
                $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
            // $count = count($uniqueRack); //untuk menghitung total Rack
        }

        $locOn1 = [];
        $locOn2 = [];
        $locOn3 = [];
        $locOn4 = [];
        $locOn5 = [];
        $locOn6 = [];
        $locOn7 = [];
        $locOn10 = [];
        $locOff1 = [];
        $locOff2 = [];
        $locOff3 = [];
        $locOff4 = [];
        $locOff5 = [];
        $locOff6 = [];
        $locOff7 = [];
        $locOff10 = [];
        $QTY = [];
        foreach ($item['item'] as $v) {
            if ($v['rack'] === $Others) {
                $subLoc = $v['sub_locations'];
                $qty = $v['qty'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if ($v['locations'] == $v['rack'] . " Area Transit 7") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn1)) {
                            $locOn1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn1 = count($locOn1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff1)) {
                            $locOff1[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff1 = count($locOff1); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . " Area Transit 6") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn2)) {
                            $locOn2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn2 = count($locOn2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff2)) {
                            $locOff2[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff2 = count($locOff2); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . " Area Transit 5") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn3)) {
                            $locOn3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn3 = count($locOn3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff3)) {
                            $locOff3[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff3 = count($locOff3); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . " Area Transit 4") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn4)) {
                            $locOn4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn4 = count($locOn4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff4)) {
                            $locOff4[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff4 = count($locOff4); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . " Area Transit 3") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn5)) {
                            $locOn5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn5 = count($locOn5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff5)) {
                            $locOff5[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff5 = count($locOff5); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . " Area Transit 2") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn6)) {
                            $locOn6[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn6 = count($locOn6); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff6)) {
                            $locOff6[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff6 = count($locOff6); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['locations'] == $v['rack'] . " Area Transit 1") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn7)) {
                            $locOn7[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn7 = count($locOn7); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff7)) {
                            $locOff7[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff7 = count($locOff7); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }

                if ($v['locations'] == $v['rack'] . " Gedung D") {
                    if ($v['code_qr'] == NULL) {
                        if (!in_array($subLoc, $locOn10)) {
                            $locOn10[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOn10 = count($locOn10); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                    if ($v['code_qr'] != NULL) {
                        if (!in_array($subLoc, $locOff10)) {
                            $locOff10[] = $subLoc; //tambahkan ke array uniqueSupplierNames
                        } else {
                            continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                        }
                        $countOff10 = count($locOff10); //untuk menghitung total Rack
                    } // jika item cocok dengan kriteria yang diinginkan
                }
                if ($v['code_qr'] != NULL) {

                    $QTY[]  = $qty;

                    $countQTYOthers = 0;
                    for ($i = 0; $i < count($QTY); $i++) {
                        $countQTYOthers += $QTY[$i];
                    }
                } // jika item cocok dengan kriteria yang diinginkan

            }
        }
        if (empty($QTY)) {
            $countQTYOthers = 0;
        }
        if (empty($locOn1)) {
            $countOn1 = 0;
        }
        if (empty($locOff1)) {
            $countOff1 = 0;
        }
        if (empty($locOn2)) {
            $countOn2 = 0;
        }
        if (empty($locOff2)) {
            $countOff2 = 0;
        }
        if (empty($locOn3)) {
            $countOn3 = 0;
        }
        if (empty($locOff3)) {
            $countOff3 = 0;
        }
        if (empty($locOn4)) {
            $countOn4 = 0;
        }
        if (empty($locOff4)) {
            $countOff4 = 0;
        }
        if (empty($locOn5)) {
            $countOn5 = 0;
        }
        if (empty($locOff5)) {
            $countOff5 = 0;
        }
        if (empty($locOn6)) {
            $countOn6 = 0;
        }
        if (empty($locOff6)) {
            $countOff6 = 0;
        }
        if (empty($locOn7)) {
            $countOn7 = 0;
        }
        if (empty($locOff7)) {
            $countOff7 = 0;
        }
        if (empty($locOn10)) {
            $countOn10 = 0;
        }
        if (empty($locOff10)) {
            $countOff10 = 0;
        }
        $totalCountOffOthers = ($countOff1 + $countOff2 + $countOff3 + $countOff4 + $countOff5 + $countOff6 + $countOff7 + $countOff10);
        $totalCountOnOthers = ($countOn1 + $countOn2 + $countOn3 + $countOn4 + $countOn5 + $countOn6 + $countOn7 + $countOn10);

        $rackOthers = [
            'totalOnOthers' => $totalCountOnOthers,
            'totalOffOthers' => $totalCountOffOthers,
            'totalQTYOthers' => ($countQTYOthers + $countQTYRecehOthers),
        ];

        return $rackOthers;
    }

    public function index()
    {
        $dataRackA = $this->countRackA();
        $rackA = [
            'totalOnA' => $dataRackA['totalOnA'],
            'totalOffA' => $dataRackA['totalOffA'],
            'totalQTYA' => $dataRackA['totalQTYA'],
        ];
        $dataRackB = $this->countRackB();
        $rackB = [
            'totalOnB' => $dataRackB['totalOnB'],
            'totalOffB' => $dataRackB['totalOffB'],
            'totalQTYB' => $dataRackB['totalQTYB'],
        ];
        $dataRackC = $this->countRackC();
        $rackC = [
            'totalOnC' => $dataRackC['totalOnC'],
            'totalOffC' => $dataRackC['totalOffC'],
            'totalQTYC' => $dataRackC['totalQTYC'],
        ];
        $dataRackD = $this->countRackD();
        $rackD = [
            'totalOnD' => $dataRackD['totalOnD'],
            'totalOffD' => $dataRackD['totalOffD'],
            'totalQTYD' => $dataRackD['totalQTYD'],
        ];
        $dataRackE = $this->countRackE();
        $rackE = [
            'totalOnE' => $dataRackE['totalOnE'],
            'totalOffE' => $dataRackE['totalOffE'],
            'totalQTYE' => $dataRackE['totalQTYE'],
        ];
        $dataRackF = $this->countRackF();
        $rackF = [
            'totalOnF' => $dataRackF['totalOnF'],
            'totalOffF' => $dataRackF['totalOffF'],
            'totalQTYF' => $dataRackF['totalQTYF'],
        ];
        $dataRackG = $this->countRackG();
        $rackG = [
            'totalOnG' => $dataRackG['totalOnG'],
            'totalOffG' => $dataRackG['totalOffG'],
            'totalQTYG' => $dataRackG['totalQTYG'],
        ];
        $dataRackH = $this->countRackH();
        $rackH = [
            'totalOnH' => $dataRackH['totalOnH'],
            'totalOffH' => $dataRackH['totalOffH'],
            'totalQTYH' => $dataRackH['totalQTYH'],
        ];
        $dataRackI = $this->countRackI();
        $rackI = [
            'totalOnI' => $dataRackI['totalOnI'],
            'totalOffI' => $dataRackI['totalOffI'],
            'totalQTYI' => $dataRackI['totalQTYI'],
        ];
        $dataRackJ = $this->countRackJ();
        $rackJ = [
            'totalOnJ' => $dataRackJ['totalOnJ'],
            'totalOffJ' => $dataRackJ['totalOffJ'],
            'totalQTYJ' => $dataRackJ['totalQTYJ'],
        ];
        $dataRackK = $this->countRackK();
        $rackK = [
            'totalOnK' => $dataRackK['totalOnK'],
            'totalOffK' => $dataRackK['totalOffK'],
            'totalQTYK' => $dataRackK['totalQTYK'],
        ];
        $dataRackL = $this->countRackL();
        $rackL = [
            'totalOnL' => $dataRackL['totalOnL'],
            'totalOffL' => $dataRackL['totalOffL'],
            'totalQTYL' => $dataRackL['totalQTYL'],
        ];
        $dataRackM = $this->countRackM();
        $rackM = [
            'totalOnM' => $dataRackM['totalOnM'],
            'totalOffM' => $dataRackM['totalOffM'],
            'totalQTYM' => $dataRackM['totalQTYM'],
        ];
        $dataRackN = $this->countRackN();
        $rackN = [
            'totalOnN' => $dataRackN['totalOnN'],
            'totalOffN' => $dataRackN['totalOffN'],
            'totalQTYN' => $dataRackN['totalQTYN'],
        ];

        $dataRackOthers = $this->countRackOthers();
        $rackOthers = [
            'totalOnOthers' => $dataRackOthers['totalOnOthers'],
            'totalOffOthers' => $dataRackOthers['totalOffOthers'],
            'totalQTYOthers' => $dataRackOthers['totalQTYOthers'],
        ];

        $allCountOff = ($rackA['totalOffA'] + $rackB['totalOffB'] + $rackC['totalOffC'] + $rackD['totalOffD'] + $rackE['totalOffE'] + $rackF['totalOffF'] + $rackG['totalOffG'] + $rackH['totalOffH'] + $rackI['totalOffI'] + $rackJ['totalOffJ'] + $rackK['totalOffK'] + $rackL['totalOffL'] + $rackM['totalOffM'] + $rackN['totalOffN'] + $rackOthers['totalOffOthers']);
        $allCountOn = ($rackA['totalOnA'] + $rackB['totalOnB'] + $rackC['totalOnC'] + $rackD['totalOnD'] + $rackE['totalOnE'] + $rackF['totalOnF'] + $rackG['totalOnG'] + $rackH['totalOnH'] + $rackI['totalOnI'] + $rackJ['totalOnJ'] + $rackK['totalOnK'] + $rackL['totalOnL'] + $rackM['totalOnM'] + $rackN['totalOnN'] + $rackOthers['totalOffOthers']);
        $allCountQTY = ($rackA['totalQTYA'] + $rackB['totalQTYB'] + $rackC['totalQTYC'] + $rackD['totalQTYD'] + $rackE['totalQTYE'] + $rackF['totalQTYF'] + $rackG['totalQTYG'] + $rackH['totalQTYH'] + $rackI['totalQTYI'] + $rackJ['totalQTYJ'] + $rackK['totalQTYK'] + $rackL['totalQTYL'] + $rackM['totalQTYM'] + $rackN['totalQTYN'] + $rackOthers['totalOffOthers']);

        $total = [
            'allCountOff' => $allCountOff,
            'allCountOn' => $allCountOn,
            'allCountQTY' => $allCountQTY
        ];

        $data = [
            'title' => 'List Item',
            'rackCA' => $rackA,
            'rackCB' => $rackB,
            'rackCC' => $rackC,
            'rackCD' => $rackD,
            'rackCE' => $rackE,
            'rackCF' => $rackF,
            'rackCG' => $rackG,
            'rackCH' => $rackH,
            'rackCI' => $rackI,
            'rackCJ' => $rackJ,
            'rackCK' => $rackK,
            'rackCL' => $rackL,
            'rackCM' => $rackM,
            'rackCN' => $rackN,
            'rackCOthers' => $rackOthers,
            'total' => $total,
            'item' => $this->tItems->getItem(),
            'itemOut' => $this->tOut->getItem(),
            'itemReceh' => $this->tReceh->getItem(),
        ];

        return view('pages/index', $data);
    }

    public function landingPage()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/landingPage', $data);
    }
}
