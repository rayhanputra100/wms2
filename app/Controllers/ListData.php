<?php

namespace App\Controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use App\Models\TItems;
use App\Models\TBackup;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class ListData extends BaseController
{
    protected $tItems;
    protected $tBackup;
    public function __construct()
    {
        $this->tItems = new TItems();
        $this->tBackup = new TBackup();
    }

    public function exportExcel()
    {
        $spreadsheet = new Spreadsheet();
        $data['item'] = $this->tItems->getItem();
        
        $activeWorksheet = $spreadsheet->getActiveSheet();

        $activeWorksheet->getColumnDimension('A')->setWidth(34, 'px');
        $activeWorksheet->getColumnDimension('B')->setWidth(196, 'px');
        $activeWorksheet->getColumnDimension('C')->setWidth(74, 'px');
        $activeWorksheet->getColumnDimension('D')->setWidth(74, 'px');
        $activeWorksheet->getColumnDimension('E')->setWidth(202, 'px');
        $activeWorksheet->getColumnDimension('F')->setWidth(34, 'px');
        $activeWorksheet->getColumnDimension('G')->setWidth(84, 'px');
        $activeWorksheet->getColumnDimension('H')->setWidth(57, 'px');
        $activeWorksheet->getColumnDimension('I')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('J')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('K')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('L')->setWidth(192, 'px');
        $activeWorksheet->getColumnDimension('M')->setWidth(84, 'px');

        $activeWorksheet->setCellValue('A1', 'NO');
        $activeWorksheet->setCellValue('B1', 'NAME CUSTOMER');
        $activeWorksheet->setCellValue('C1', 'NO RFQ');
        $activeWorksheet->setCellValue('D1', 'NO WO');
        $activeWorksheet->setCellValue('E1', 'NAME ITEM');
        $activeWorksheet->setCellValue('F1', 'QTY');
        $activeWorksheet->setCellValue('G1', 'WAREHOUSE');
        $activeWorksheet->setCellValue('H1', 'NO TAG');
        $activeWorksheet->setCellValue('I1', 'NO SDF');
        $activeWorksheet->setCellValue('J1', 'BPID');
        $activeWorksheet->setCellValue('K1', 'LOT DELIVERY');
        $activeWorksheet->setCellValue('L1', 'CODE QR');
        $activeWorksheet->setCellValue('M1', 'LOCATIONS');

        $row = 2;
        $no = 1;

        foreach ($data['item'] as $v) {
            if ($v['no_rfq'] != NULL) {
                $activeWorksheet->setCellValue('A' . $row, $no++);
                $activeWorksheet->setCellValue('B' . $row, $v['name_cust']);
                $activeWorksheet->setCellValue('C' . $row, $v['no_rfq']);
                $activeWorksheet->setCellValue('D' . $row, $v['no_wo']);
                $activeWorksheet->setCellValue('E' . $row, $v['name_item']);
                $activeWorksheet->setCellValue('F' . $row, $v['qty']);
                $activeWorksheet->setCellValue('G' . $row, $v['warehouse']);
                $activeWorksheet->setCellValue('H' . $row, $v['no_tag']);
                $activeWorksheet->setCellValue('I' . $row, $v['no_sdf']);
                $activeWorksheet->setCellValue('J' . $row, $v['bpid']);
                $activeWorksheet->setCellValue('K' . $row, $v['lot_del']);
                $activeWorksheet->setCellValue('L' . $row, $v['code_qr']);
                $activeWorksheet->setCellValue('M' . $row, ($v['locations'].".".$v['sub_locations']));
                $row++;
            }
        }
        // Set the header content type and attachment filename
        

        $filename = 'Warehouser-Report at - ' . date('Y-m-d') . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }

    public function index()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/listData', $data);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tItems->getItem($id),
        ];
        return view('pages/detail', $data);
    }

    public function json($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tItems->getItem($id),
        ];
        return $this->response->setJSON($data);
    }



    public function qr($id)
    {

        $writer = new PngWriter();

        // Create QR code
        $qrCode = QrCode::create('http://localhost:8080/json/' . $id)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        $result->saveToFile('assets/images/qrcode/listdata/item-' . $id . '.png');
        return redirect()->to('/listdata');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Ubah Data Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tItems->getItem($id)

        ];

        return view('pages/form/formEditData', $data);
    }

    public function delete($id)
    {
        $this->tItems->delete($id);

        return redirect()->to('/admin');
    }

    public function reset($id)
    {
        $this->tItems->update($id, array('no_sdf' => "", 'lot_del' => "", 'no_rfq' => "", 'no_wo' => "", 'name_cust' => "", 'code_qr' => "", 'qty' => "", 'warehouse' => "", 'no_tag' => "", 'name_item' => "", 'desc_pn' => "", 'bpid' => ""));
        return redirect()->to('/admin');
    }
}
