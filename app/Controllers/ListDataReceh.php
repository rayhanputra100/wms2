<?php

namespace App\Controllers;

use App\Models\TReceh;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class ListDataReceh extends BaseController
{
    protected $tReceh;
    public function __construct()
    {
        $this->tReceh = new TReceh();
    }

    public function index()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tReceh->getItem(),
        ];
        return view('pages/listDataReceh', $data);
    }

    public function exportExcel()
    {
        $spreadsheet = new Spreadsheet();
        $data['item'] = $this->tReceh->getItem();
        
        $activeWorksheet = $spreadsheet->getActiveSheet();

        $activeWorksheet->getColumnDimension('A')->setWidth(34, 'px');
        $activeWorksheet->getColumnDimension('B')->setWidth(196, 'px');
        $activeWorksheet->getColumnDimension('C')->setWidth(74, 'px');
        $activeWorksheet->getColumnDimension('D')->setWidth(74, 'px');
        $activeWorksheet->getColumnDimension('E')->setWidth(202, 'px');
        $activeWorksheet->getColumnDimension('F')->setWidth(34, 'px');
        $activeWorksheet->getColumnDimension('G')->setWidth(84, 'px');
        $activeWorksheet->getColumnDimension('H')->setWidth(57, 'px');
        $activeWorksheet->getColumnDimension('I')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('J')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('K')->setWidth(95, 'px');
        $activeWorksheet->getColumnDimension('L')->setWidth(192, 'px');
        $activeWorksheet->getColumnDimension('M')->setWidth(84, 'px');

        $activeWorksheet->setCellValue('A1', 'NO');
        $activeWorksheet->setCellValue('B1', 'NAME CUSTOMER');
        $activeWorksheet->setCellValue('C1', 'NO RFQ');
        $activeWorksheet->setCellValue('D1', 'NO WO');
        $activeWorksheet->setCellValue('E1', 'NAME ITEM');
        $activeWorksheet->setCellValue('F1', 'QTY');
        $activeWorksheet->setCellValue('G1', 'WAREHOUSE');
        $activeWorksheet->setCellValue('H1', 'NO TAG');
        $activeWorksheet->setCellValue('I1', 'NO SDF');
        $activeWorksheet->setCellValue('J1', 'BPID');
        $activeWorksheet->setCellValue('K1', 'LOT DELIVERY');
        $activeWorksheet->setCellValue('L1', 'CODE QR');
        $activeWorksheet->setCellValue('M1', 'LOCATIONS');

        $row = 2;
        $no = 1;

        foreach ($data['item'] as $v) {
            if ($v['no_rfq_receh'] != NULL && $v['no_rfq_receh'] != 'dummy123') {
                $activeWorksheet->setCellValue('A' . $row, $no++);
                $activeWorksheet->setCellValue('B' . $row, $v['name_cust_receh']);
                $activeWorksheet->setCellValue('C' . $row, $v['no_rfq_receh']);
                $activeWorksheet->setCellValue('D' . $row, $v['no_wo_receh']);
                $activeWorksheet->setCellValue('E' . $row, $v['name_item_receh']);
                $activeWorksheet->setCellValue('F' . $row, $v['qty_receh']);
                $activeWorksheet->setCellValue('G' . $row, $v['warehouse_receh']);
                $activeWorksheet->setCellValue('H' . $row, $v['no_tag_receh']);
                $activeWorksheet->setCellValue('I' . $row, $v['no_sdf_receh']);
                $activeWorksheet->setCellValue('J' . $row, $v['bpid_receh']);
                $activeWorksheet->setCellValue('K' . $row, $v['lot_del_receh']);
                $activeWorksheet->setCellValue('L' . $row, $v['code_qr_receh']);
                $activeWorksheet->setCellValue('M' . $row, ($v['locations_receh'].".".$v['sub_locations_receh']));
                $row++;
            }
        }
        // Set the header content type and attachment filename
        

        $filename = 'Warehouser-Report-Receh at - ' . date('Y-m-d') . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tReceh->getItem($id),
        ];
        return view('pages/detail', $data);
    }

    public function json($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tReceh->getItem($id),
        ];
        return $this->response->setJSON($data);
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Ubah Data Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tReceh->getItem($id)

        ];

        return view('pages/form/formEditData', $data);
    }

    public function delete($id)
    {
        $this->tReceh->delete($id);

        return redirect()->to('/admin');
    }

    public function reset($id)
    {
        $this->tReceh->update($id, array('no_sdf' => "",'lot_del' => "",'no_rfq' => "", 'no_wo' => "", 'name_cust' => "", 'code_qr' => "", 'qty' => "",'warehouse' => "",'no_tag' => "",'name_item' => "",'desc_pn' => "",'bpid' => ""));
        return redirect()->to('/admin'); 
    }
}
