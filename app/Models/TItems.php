<?php

namespace App\Models;

use CodeIgniter\Model;

class TItems extends Model
{
    protected $table            = 'TItems';
    protected $allowedFields    = ['no_rfq', 'no_wo', 'name_cust','no_sdf','lot_del', 'qty', 'code_qr', 'locations','sub_locations', 'rack','warehouse','no_tag','name_item','name_item_receh','desc_pn','bpid'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false) {
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }

}