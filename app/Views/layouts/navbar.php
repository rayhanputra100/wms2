<!-- Topbar Start -->
<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-end mb-0">

            <li class="dropdown d-none d-lg-inline-block">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                    <i class="fe-maximize noti-icon"></i>
                </a>
            </li>
            
            <!-- <li class="dropdown notification-list">
                <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                    <i class="fe-settings noti-icon"></i>
                </a>
            </li> -->

        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="index.html" class="logo logo-dark text-center">
                <span class="logo-sm">
                    <img src="../assets/images/iPTCBI.png" alt="" height="22">
                    <!-- <span class="logo-lg-text-light">UBold</span> -->
                </span>
                <span class="logo-lg">
                    <img src="../assets/images/iPTCBI.png" alt="" height="20">
                    <!-- <span class="logo-lg-text-light">U</span> -->
                </span>
            </a>

            <a href="index.html" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img src="../assets/images/iPTCBI.png" alt="" height="22">
                </span>
                <span class="logo-lg">
                    <img src="../assets/images/iPTCBI.png" alt="" height="60">
                </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>

            <li>
                <!-- Mobile menu toggle (Horizontal Layout)-->
                <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- end Topbar -->

<div class="topnav">
    <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-dashboard" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fe-airplay me-1"></i> Dashboards <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="topnav-dashboard">
                            <a href="/" class="dropdown-item">Home</a>
                            <a href="/homeV2" class="dropdown-item">HomeV2</a>
                            <a href="/admin" class="dropdown-item">Admin(All Rack)</a>
                            <a href="/admin/scan" class="dropdown-item">Form Input Data From Scan</a>
                            <a href="/admin/create" class="dropdown-item">Form Input Rack & Location</a>
                            <!-- <a href="/adminqr" class="dropdown-item">Form Input & Generate QR-Code</a> -->
                            <a href="/listdata" class="dropdown-item">List Data</a>
                        </div>
                    </li>

                </ul> <!-- end navbar-->
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-dashboard" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-warehouse"></i> Rack WH <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="topnav-dashboard">
                            <a href="/rackA" class="dropdown-item">Rack A</a>
                            <a href="/rackB" class="dropdown-item">Rack B</a>
                            <a href="/rackC" class="dropdown-item">Rack C</a>
                            <a href="/rackD" class="dropdown-item">Rack D</a>
                            <a href="/rackE" class="dropdown-item">Rack E</a>
                            <a href="/rackF" class="dropdown-item">Rack F</a>
                            <a href="/rackG" class="dropdown-item">Rack G</a>
                            <a href="/rackH" class="dropdown-item">Rack H</a>
                            <a href="/rackI" class="dropdown-item">Rack I</a>
                            <a href="/rackJ" class="dropdown-item">Rack J</a>
                            <a href="/rackK" class="dropdown-item">Rack K</a>
                            <a href="/rackL" class="dropdown-item">Rack L</a>
                            <a href="/rackM" class="dropdown-item">Rack M</a>
                            <a href="/rackN" class="dropdown-item">Rack N</a>
                            <a href="/rackOthers" class="dropdown-item">Rack Others</a>
                        </div>
                    </li>

                </ul> <!-- end navbar-->
            </div> <!-- end .collapsed-->
        </nav>
    </div> <!-- end container-fluid -->
</div> <!-- end topnav-->