<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
        <div class="row">
    <div class="col-xl-8 col-lg-7">
        <!-- project card -->
        <div class="card d-block">
            <div class="card-body">

                <div class="float-sm-end mb-2 mb-sm-0">
                    <div class="row g-2">
                        <div class="col-auto">
                            <a href="/admin" class="btn btn-sm btn-link"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
                        </div>
                    </div>
                </div> <!-- end dropdown-->

                <h4 class="mb-3 mt-0 font-18">Detail Item</h4>

                <div class="clerfix"></div>

                <div class="row">
                    <div class="col-md-6">
                        <label class="mt-2 mb-1">Name Customer :</label>
                        <p>
                            <?= $item['name_cust']; ?>
                        </p>
                    </div>
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-md-6">
                        <!-- Reported by -->
                        <label class="mt-2 mb-1">No RFQ :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['no_rfq']; ?> </p>
                            </div>
                        </div>
                        <!-- end Reported by -->
                    </div> <!-- end col -->

                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">No WO :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['no_wo']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">QTY :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['qty']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->

                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">No Tag :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['no_tag']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-md-6">
                        <label class="mt-2 mb-1">Warehouse :</label>
                        <p>
                            <?= $item['warehouse']; ?>
                        </p>
                    </div>
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">BPID :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['bpid']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->

                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">Note :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['code_qr']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">Item :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['name_item']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->

                    <div class="col-md-6">
                        <!-- assignee -->
                        <label class="mt-2 mb-1">Description PN :</label>
                        <div class="d-flex align-items-start">
                            <div class="w-100">
                                <p> <?= $item['desc_pn']; ?> </p>
                            </div>
                        </div>
                        <!-- end assignee -->
                    </div> <!-- end col -->
                </div> <!-- end row -->
                
                <div class="row">
                    <div class="col-md-6">
                        <!-- Status -->
                        <label class="mt-2 mb-1">Created On :</label>
                        <p><?= date_format(date_create($item['created_at']), 'd-M-Y'); ?> <small class="text-muted"><?= date_format(date_create($item['created_at']), 'h:i A'); ?></small></p>
                        <!-- end Status -->
                    </div> <!-- end col -->
                    <div class="col-md-6">
                        <!-- Status -->
                        <label class="mt-2 mb-1">Update On :</label>
                        <p><?= date_format(date_create($item['updated_at']), 'd-M-Y'); ?> <small class="text-muted"><?= date_format(date_create($item['updated_at']), 'h:i A'); ?></small></p>
                        <!-- end Status -->
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end card-body-->

        </div> <!-- end card-->
    </div>
</div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>