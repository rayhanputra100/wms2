<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = [];
$uniqueLoc = [];

foreach ($item as $v) {
    $rack = $v['rack'];
    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack;
    } else {
        continue;
    }
}
foreach ($item as $v) {
    $loc = $v['locations'];
    if (!in_array($loc, $uniqueLoc)) {
        $uniqueLoc[] = $loc;
    } else {
        continue;
    }
}
?>

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Form </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Form Input Rack Location</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Form Input Rack Location</h4>
                            <p class="sub-header">This form for input rack and location</p>
                            <form action="/admin/save" method="post" class="needs-validation" novalidate>
                                <?= csrf_field(); ?>
                                <div class="mb-3">
                                    <label for="rack" class="form-label">Rack</label>
                                    <select class="form-control" id="rack" placeholder="rack" name="rack" require>
                                    <option value="">--SELECT RACK--</option>
                                        <?php foreach ($uniqueRack as $v) : ?>
                                            <option value="<?= $v; ?>"><?= $v; ?></option>
                                        <?php endforeach ?>
                                    </select>

                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="location" class="form-label">Location</label>
                                    <select class="form-control" id="locations" placeholder="Locations" name="locations" require>
                                    </select>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="sub_location" class="form-label">Sub Location</label>
                                    <input type="number" class="form-control <?= ($validation->hasError('sub_locations')) ? 'is-invalid' : ''; ?>" id="sub_locations" placeholder="Sub Locations" name="sub_locations" value="<?= old('sub_locations'); ?>" required/>
                                    <?php if($validation->hasError('sub_locations')) : ?>
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('sub_locations'); ?>
                                    </div>
                                    <?php endif ?>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <script>
                                    const rackSelect = document.getElementById("rack");
                                    const locSelect = document.getElementById("locations");

                                    rackSelect.addEventListener("change", function() {
                                        // Filter unique locations based on the selected rack
                                        const selectedRack = rackSelect.value;
                                        const filteredLocs = <?php echo json_encode($uniqueLoc); ?>.filter(loc => loc.startsWith(selectedRack));

                                        // Clear existing options in the locations select
                                        locSelect.innerHTML = "";

                                        // Add filtered options to the locations select
                                        filteredLocs.forEach(loc => {
                                            const option = document.createElement("option");
                                            option.value = loc;
                                            option.text = loc;
                                            locSelect.add(option);
                                        });
                                    });
                                </script>
                                
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


    <?= $this->endSection(); ?>