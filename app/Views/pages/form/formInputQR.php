<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Form </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Form Input QR</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Form Input QR</h4>
                            <p class="sub-header">This form for input QR</p>

                            <form action="/adminqr/save" method="post" class="needs-validation" novalidate>
                                <?= csrf_field(); ?>
                                <div class="mb-3">
                                    <label for="no_rfq" class="form-label">No RFQ</label>
                                    <input type="text" class="form-control" id="no_rfq" placeholder="No RFQ" name="no_rfq" autofocus value="<?= old('no_rfq'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="no_wo" class="form-label">No WO</label>
                                    <input type="text" class="form-control" id="no_wo" placeholder="No WO" name="no_wo" value="<?= old('no_wo'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="name_cust" class="form-label">Name Customer</label>
                                    <input type="text" class="form-control" id="name_cust" placeholder="Name Customer" name="name_cust" value="<?= old('name_cust'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="code_qr" class="form-label">Code QR</label>
                                    <input type="text" class="form-control" id="code_qr" placeholder="Code_QR" name="code_qr" value="<?= old('code_qr'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="qty" class="form-label">QTY</label>
                                    <input type="number" class="form-control" id="qty" placeholder="QTY" name="qty" value="<?= old('qty'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="warehouse" class="form-label">Warehouse</label>
                                    <input type="text" class="form-control" id="warehouse" placeholder="Warehouse" name="warehouse" value="<?= old('warehouse'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="name_item" class="form-label">Name Item</label>
                                    <input type="text" class="form-control" id="name_item" placeholder="Name Item" name="name_item" value="<?= old('name_item'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="no_tag" class="form-label">No Tag</label>
                                    <input type="number" class="form-control" id="no_tag" placeholder="No_Tag" name="no_tag" value="<?= old('no_tag'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="desc_pn" class="form-label">Description PN</label>
                                    <input type="text" class="form-control" id="desc_pn" placeholder="Description PN" name="desc_pn" value="<?= old('desc_pn'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="bpid" class="form-label">BPID</label>
                                    <input type="text" class="form-control" id="bpid" placeholder="BPID" name="bpid" value="<?= old('bpid'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Get QR-Code</h4>
                            <p class="sub-header">This form for get QR-Code</p>
                            <div class="mb-3">
                                <?php foreach ($item as $v) : ?>
                                    <div class="row g-0">
                                        <div class="col-md-4">
                                            <img src="../assets/images/qrcode/item-<?= $v["id"]; ?>.png" alt="qritem-<?= $v["id"]; ?>" class="img-fluid">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title"><?= $v['name_cust']; ?></h5>
                                                <p class="card-text"><?= $v['no_rfq']; ?></p>
                                                <p class="card-text"><?= $v['no_wo']; ?></p>
                                                <p class="card-text"><?= $v['qty']; ?></p>
                                                <p class="card-text"><?= $v['code_qr']; ?></p>
                                                <a class="btn btn-xs btn-success me-1" href="/adminqr/detail/<?= $v['id']; ?>">Detail</a>
                                                <a class="btn btn-xs btn-success me-1" href="/adminqr/json/<?= $v['id']; ?>">Link Json</a>
                                                <a class="btn btn-xs btn-success me-1" href="/adminqr/qr/<?= $v['id']; ?>">Generate QR</a>
                                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                <?php endforeach ?>
                            </div>


                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div> <!-- end row-->
        </div> <!-- container -->

    </div> <!-- content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


<?= $this->endSection(); ?>