<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = [];
$uniqueLoc = [];


foreach ($item as $v) {
    $rack = $v['rack'];
    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack;
    } else {
        continue;
    }
}
foreach ($item as $v) {
    $loc = $v['locations'];
    if (!in_array($loc, $uniqueLoc)) {
        $uniqueLoc[] = $loc;
    } else {
        continue;
    }
}

?>

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid mb-0">

            <!-- start page title -->
            <!-- <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Form </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Form Update Item</h4>
                    </div>
                </div>
            </div> -->
            <!-- end page title -->

            <div class="row">
                <h4 class="page-title">Form Input Item</h4>
                <?php if (session()->getFlashdata('pesan')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->getFlashdata('pesan'); ?>
                    </div>
                <?php endif; ?>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Form Input Item</h4>

                            <form action="/admin/scandat" method="post" class="needs-validation" novalidate>
                                <?= csrf_field(); ?>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="no_rfq" class="form-label">No RFQ</label>
                                            <input type="text" class="form-control" id="no_rfq" placeholder="No RFQ" name="no_rfq" readonly />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="no_wo" class="form-label">No WO</label>
                                            <input type="text" class="form-control" id="no_wo" placeholder="No WO" name="no_wo" readonly />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div hidden class="mb-3">
                                    <label for="name_cust" class="form-label">Name Customer</label>
                                    <input type="text" class="form-control" id="name_cust" placeholder="Name Customer" name="name_cust" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" >
                                    <label for="code_qr" class="form-label">Code QR</label>
                                    <input type="text" class="form-control" id="code_qr" placeholder="Code_QR" name="code_qr" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="name_item" class="form-label">Name Item</label>
                                            <input type="text" class="form-control" id="name_item" placeholder="Name Item" name="name_item" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="mb-3">

                                    <label for="qty" class="form-label">QTY</label>
                                    <input type="number" class="form-control" id="qty" placeholder="QTY" name="qty" readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>

                                </div>
                                <div hidden class="mb-3">
                                    <label for="lot_del" class="form-label">Lot Delivery</label>
                                    <input type="text" class="form-control" id="lot_del" placeholder="Lot Devlivery" name="lot_del" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="no_sdf" class="form-label">No SDF</label>
                                    <input type="text" class="form-control" id="no_sdf" placeholder="No SDF" name="no_sdf" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="warehouse" class="form-label">Warehouse</label>
                                    <input type="text" class="form-control" id="warehouse" placeholder="Warehouse" name="warehouse" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="no_tag" class="form-label">No Tag</label>
                                    <input type="number" class="form-control" id="no_tag" placeholder="No_Tag" name="no_tag" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="desc_pn" class="form-label">Description PN</label>
                                    <input type="text" class="form-control" id="desc_pn" placeholder="Description PN" name="desc_pn" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="bpid" class="form-label">BPID</label>
                                    <input type="text" class="form-control" id="bpid" placeholder="BPID" name="bpid" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <label for="rack" class="form-label">Rack</label>
                                        <select class="form-control" id="rack" placeholder="rack" name="rack" require>
                                            <?php foreach ($uniqueRack as $v) : ?>
                                                <option value="<?= $v; ?>"><?= $v; ?></option>
                                            <?php endforeach ?>
                                        </select>

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="location" class="form-label">Location</label>
                                        <select class="form-control" id="locations" placeholder="Locations" name="locations" require>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="col">
                                        <label for="sub_location" class="form-label">Sub Location</label>
                                        <select class="form-control" id="sub_locations" placeholder="Sub Locations" name="sub_locations" require>
                                        </select>
                                        <?php if ($validation->hasError('sub_locations')) : ?>
                                            <div class="invalid-feedback">
                                                <?= $validation->getError('sub_locations'); ?>
                                            </div>
                                        <?php endif ?>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>

                                </div>

                                <div hidden class="mb-3">
                                    <label for="id" class="form-label">Id</label>
                                    <select class="form-control" id="id" placeholder="ID" name="id" require>
                                    </select>

                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <script>
                                    const rackSelect = document.getElementById("rack");
                                    const locSelect = document.getElementById("locations");
                                    const subLocSelect = document.getElementById("sub_locations");
                                    const idSelect = document.getElementById("id");
                                    const uniqueLocs = <?php echo json_encode($uniqueLoc); ?>;
                                    const items = <?php echo json_encode($item); ?>;

                                    function filterLocationsByRack() {
                                        const selectedRack = rackSelect.value;
                                        const filteredLocs = uniqueLocs.filter(loc => loc.startsWith(selectedRack));
                                        const filteredLocsWithEmptySubLocs = filteredLocs.filter(loc => {
                                            const subLocs = items.filter(item => item.locations === loc && (item.code_qr === "" || item.code_qr === null));
                                            return subLocs;
                                        });

                                        // New implementation starts here
                                        const ids = filteredLocsWithEmptySubLocs.map(loc => {
                                            const item = items.find(item => item.locations === loc && (item.code_qr === "" || item.code_qr === null));
                                            return item ? item.id : null;
                                        });
                                        const uniqueIds = [...new Set(ids)];
                                        const filteredItems = items.filter(item => uniqueIds.includes(item.id));
                                        const locationsToPrint = [...new Set(filteredItems.map(item => item.locations))];
                                        // New implementation ends here

                                        clearOptions(locSelect);
                                        addOptions(locSelect, locationsToPrint);
                                        triggerChangeEvent(locSelect);
                                    }


                                    function filterSubLocationsByLocation() {
                                        const selectedLoc = locSelect.value;
                                        const filteredSubLocs = items.filter(item => item.locations === selectedLoc && (item.code_qr === "" || item.code_qr === null));

                                        // Menampilkan ID item jika tidak memiliki QR code
                                        filteredSubLocs.forEach(item => {
                                            if (item.code_qr === "" || item.code_qr === null) {
                                                return item.id;
                                            }
                                        });

                                        clearOptions(subLocSelect);
                                        addOptions(subLocSelect, filteredSubLocs.map(item => item.sub_locations));
                                        triggerChangeEvent(subLocSelect);
                                    }

                                    function filterIdsBySubLocationAndLocation() {
                                        const selectedSubLoc = parseInt(subLocSelect.value);
                                        const selectedLoc = locSelect.value;
                                        const filteredIds = items.filter(item => item.sub_locations === selectedSubLoc && item.locations === selectedLoc && (item.code_qr === "" || item.code_qr === null));

                                        // Add logic to display item.id if code_qr is empty or null and sort the ids in ascending order
                                        const sortedIdsToDisplay = filteredIds.filter(item => item.code_qr === "" || item.code_qr === null)
                                            .sort((a, b) => a.id - b.id)
                                            .map(item => item.id);

                                        clearOptions(idSelect);
                                        addOptions(idSelect, sortedIdsToDisplay);
                                        triggerChangeEvent(idSelect);

                                        // Select the first ID option by default
                                        if (sortedIdsToDisplay.length > 0) {
                                            idSelect.value = sortedIdsToDisplay[0];
                                            // Trigger change event for id select to display the selected item data
                                            triggerChangeEvent(idSelect);
                                        }
                                    }




                                    function clearOptions(selectElement) {
                                        selectElement.innerHTML = "";
                                    }

                                    function addOptions(selectElement, options) {
                                        options.forEach(option => {
                                            const optionElement = document.createElement("option");
                                            optionElement.value = option;
                                            optionElement.text = option;
                                            selectElement.add(optionElement);
                                        });
                                    }

                                    function triggerChangeEvent(selectElement) {
                                        const event = new Event('change');
                                        selectElement.dispatchEvent(event);
                                    }

                                    rackSelect.addEventListener("change", filterLocationsByRack);
                                    locSelect.addEventListener("change", filterSubLocationsByLocation);
                                    subLocSelect.addEventListener("change", filterIdsBySubLocationAndLocation);
                                    idSelect.addEventListener("change", function() {
                                        // Ambil data item berdasarkan id
                                        const selectedItem = items.find(item => item.id === parseInt(idSelect.value));
                                        console.log(selectedItem);
                                    });


                                    // Trigger change event for rack select when the page first loads
                                    triggerChangeEvent(rackSelect);
                                </script>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Scan QR-Code</h4>
                            <p class="sub-header">This form for edit item</p>
                            <!-- SCANNER -->
                            <!-- <video id="preview" src="" style="height: 250px;"></video> -->
                            <!-- SCANNER -->

                            <div class="mb-3">
                                <label for="code_qr" class="form-label">Code QR</label>
                                <input type="text" class="form-control" placeholder="Input QR-Code" id="qr" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>

                            <script type="text/javascript">
                                // variabel yang tidak berubah
                                const input = document.getElementById('qr');
                                // const scanner = new Instascan.Scanner({
                                //     video: document.getElementById('preview')
                                // });

                                // fungsi untuk menangani respons dari permintaan GET
                                function handleResponse() {
                                    if (this.readyState === 4 && this.status === 200) {
                                        const data = JSON.parse(this.responseText);
                                        if (data.results[0].NO_RFQ === null || data.results[0].NO_RFQ === "") {
                                            alert("Data kosong");
                                        } else {
                                            alert("Data ada");
                                            document.getElementById('no_rfq').value = data.results[0].NO_RFQ;
                                            document.getElementById('no_wo').value = data.results[0].NO_WO;
                                            document.getElementById('name_cust').value = data.results[0].CUSTOMER_NAME;
                                            document.getElementById('code_qr').value = data.results[0].NOTE;
                                            document.getElementById('qty').value = data.results[0].QTY;
                                            document.getElementById('warehouse').value = data.results[0].WAREHOUSE;
                                            document.getElementById('no_tag').value = data.results[0].NO_TAG;
                                            document.getElementById('name_item').value = data.results[0].ITEM;
                                            document.getElementById('desc_pn').value = data.results[0].DESCRIPTION_PN;
                                            document.getElementById('bpid').value = data.results[0].BPID;
                                            document.getElementById('no_sdf').value = data.results[0].SDF_ORDER;
                                            document.getElementById('lot_del').value = data.results[0].QTY_LOT_DELIV;
                                        }
                                    } else if (this.readyState === 4) {
                                        alert("Tidak ada koneksi");
                                    }
                                }

                                // fungsi untuk mengirimkan permintaan GET
                                function sendGetRequest(url) {
                                    const xhr = new XMLHttpRequest();
                                    xhr.open('GET', url, true);
                                    xhr.onreadystatechange = handleResponse;
                                    xhr.send();
                                }

                                // event listener untuk input
                                input.addEventListener('input', function(event) {
                                    const content = event.target.value;
                                    const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + content;
                                    sendGetRequest(url);
                                });

                                // event listener untuk scanner
                                // scanner.addListener('scan', function(content) {
                                //     const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + content;
                                //     sendGetRequest(url);
                                // });

                                // memulai scanner
                                // Instascan.Camera.getCameras().then(function(cameras) {
                                //     if (cameras.length > 0) {
                                //         scanner.start(cameras[0]);
                                //     } else {
                                //         console.error('camera tidak di temukan');
                                //     }
                                // }).catch(function(e) {
                                //     console.error(e);
                                // });
                            </script>

                            <!-- end scan qr from api -->

                            <!-- scan qr from database local -->
                            <!-- <script type="text/javascript">
                                let scanner = new Instascan.Scanner({
                                    video: document.getElementById('preview')
                                });
                                scanner.addListener('scan', function(content) {
                                    // menampilkan hasil dari scan qr code
                                    // $('#qrcode').val(content);
                                    alert(content);
                                    // membuat objek XMLHttpRequest
                                    let xhr = new XMLHttpRequest();

                                    // menentukan URL dari request
                                    let url = content;

                                    // menentukan method dan URL
                                    xhr.open('GET', url, true);

                                    // menambahkan event listener untuk menangani response
                                    xhr.onreadystatechange = function() {
                                        if (this.readyState === 4 && this.status === 200) {
                                            let data = JSON.parse(this.responseText);
                                            // menampilkan data di console
                                            document.getElementById('no_rfq').value = data.item.no_rfq;
                                            document.getElementById('no_wo').value = data.item.no_wo;
                                            document.getElementById('name_cust').value = data.item.name_cust;
                                            document.getElementById('code_qr').value = data.item.code_qr;
                                            document.getElementById('qty').value = data.item.qty;
                                            document.getElementById('warehouse').value = data.item.warehouse;
                                            document.getElementById('no_tag').value = data.item.no_tag;
                                            document.getElementById('name_item').value = data.item.name_item;
                                            document.getElementById('desc_pn').value = data.item.desc_pn;
                                            document.getElementById('bpid').value = data.item.bpid;
                                        }
                                    };

                                    xhr.send();
                                });
                                Instascan.Camera.getCameras().then(function(cameras) {
                                    if (cameras.length > 0) {
                                        scanner.start(cameras[0]);
                                    } else {
                                        console.error('camera tidak di temukan');
                                    }
                                }).catch(function(e) {
                                    console.error(e);
                                });
                            </script> -->
                            <!-- end scan qr from database local -->
                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer mt-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


    <?= $this->endSection(); ?>