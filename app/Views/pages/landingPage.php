<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <h1 class="page-title text-center">Welcome To Warehouse</h1>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="responsive-table-plugin mt-3">
                                    <div class="table-rep-plugin">
                                        <h4 class="page-title">List Rack</h4>
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="tech-companies-1" class="table table-striped">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackA">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack A</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:lightseagreen"></i>
                                                                    </div>
                                                                </a>

                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackB">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack B</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:mediumpurple"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackC">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack C</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:crimson"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackD">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack D</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:peru"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackE">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack E</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:indigo"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackF">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack F</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:lightseagreen"></i>
                                                                    </div>
                                                                </a>

                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackG">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack G</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:mediumpurple"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackH">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack H</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:crimson"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackI">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack I</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:peru"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackJ">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center"> Rack J</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:indigo"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackK">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack K</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:lightseagreen"></i>
                                                                    </div>
                                                                </a>

                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackL">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack L</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:mediumpurple"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackM">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack M</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:crimson"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackN">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Rack N</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:peru"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                        <td>
                                                            <div class="card bg-pattern">
                                                                <a href="/rackOthers">
                                                                    <div class="card-body">
                                                                        <h4 class="text-center">Others</h4>
                                                                        <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:indigo"></i>
                                                                    </div>
                                                                </a>
                                                            </div> <!-- end card-->
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div> <!-- end .table-responsive -->

                                    </div> <!-- end .table-rep-plugin-->
                                </div> <!-- end .responsive-table-plugin-->

                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div>
            <!-- end row-->


        </div> <!-- container -->

    </div> <!-- content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


</div>
<!-- END wrapper -->

<?= $this->endSection(); ?>