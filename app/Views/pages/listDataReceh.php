<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = []; //array penampung nama supplier yang sudah unik
//melakukan iterasi terhadap hasil fetch data dari supplier
foreach ($item as $v) {
    $rack = $v['rack_receh'];
    //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
    } else {
        continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
    }
    // $count = count($uniqueRack); //untuk menghitung total Rack
}


?>

<?php

use App\Models\TItems;

$itemsModel = new TItems();
$itemsResult = $itemsModel->findAll();

if (!empty($_POST)) {
    foreach ($_POST['radioReset'] as $idItems) {
        $query = $itemsModel->update($idItems, [
            'no_rfq_receh' => "",
            'no_sdf_receh' => "",
            'lot_del_receh' => "",
            'no_wo_receh' => "",
            'name_cust_receh' => "",
            'code_qr_receh' => "",
            'qty_receh' => "",
            'warehouse_receh' => "",
            'no_tag_receh' => "",
            'name_item_receh' => "",
            'desc_pn_receh' => "",
            'bpid_receh' => ""
        ]);
    }
    return redirect()->to('/listdata');
    exit;
}
?>




<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Warning Alert Modal -->
                                <div id="warning-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-body p-4">
                                                <div class="text-center">
                                                    <i class="dripicons-warning h1 text-warning"></i>
                                                    <h4 class="mt-2">Warning</h4>
                                                    <p class="mt-3">Button akan menghapus data tanpa melakukan scan QR code terlebih dahulu. Apa anda yakin ?</p>
                                                    <button type="button" class="btn btn-sm btn-light my-2" data-bs-dismiss="modal">Tidak</button>
                                                    <button id="delete-row-receh-paksa" class="btn btn-xs btn-danger me-1" disabled><i class="mdi mdi-close me-1"></i>Reset</button>
                                                </div>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                <div class="float-sm-end mb-2 mb-sm-0">
                                    <div class="row g-2">
                                        <div class="col-auto">
                                            <a href="/listdata" class="btn btn-md btn-link"><i class="mdi mdi-keyboard-backspace"></i> Back To List Data</a>
                                        </div>
                                    </div>
                                </div> <!-- end dropdown-->
                                <h4 class="header-title">List Data Warehouse</h4>
                                <!-- <button class="btn btn-success btn-sm export-btn-receh my-1"><i class="fas fa-file-excel"></i> Download</button> -->
                                <a href="/exportReceh" class="btn btn-success btn-sm my-1"><i class="fas fa-file-excel"></i> Download</a>
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <div class="btn-group mb-2 dropend">
                                        <button type="button" class="btn btn-sm btn-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilih Reset<i class="mdi mdi-chevron-right"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <button id="delete-row-receh-biasa" class="btn btn-xs btn-danger me-1" disabled><i class="mdi mdi-close me-1"></i>Reset</button>
                                            <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#warning-alert-modal"><i class="mdi mdi-close me-1"></i>Reset Paksa !</button>

                                        </div>
                                    </div>

                                    <table id="custom-toolbar-receh" data-toggle="table" data-toolbar="#demo-delete-row" data-search="true" data-sort-name="id" data-page-list="[5, 20, 50]" data-page-size="50" data-pagination="true" data-show-pagination-switch="true" class="table-borderless">
                                        <thead class="table-light">
                                            <tr class="text-center">
                                                <th data-field="state" data-checkbox="true"></th>
                                                <th>NO</th>
                                                <th>ID</th>
                                                <th>CUSTOMER</th>
                                                <th>ITEM</th>
                                                <th>NO RFQ</th>
                                                <th>NO WO</th>
                                                <th>NO SDF</th>
                                                <th>QTY</th>
                                                <th>LOT DELIVERY</th>
                                                <th>LOCATION</th>

                                            </tr>

                                        </thead>


                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($item as $v) : ?>
                                                <?php if (!empty($v['no_rfq_receh'] && $v['no_wo_receh'])) : ?>
                                                    <?php if ($v['no_rfq_receh'] != 'dummy123') : ?>
                                                        <tr class="text-center">
                                                            <td></td>
                                                            <td><?= $i++; ?></td>
                                                            <td><?= $v['id_receh']; ?></td>
                                                            <?php if ($v['name_cust_receh'] == "") : ?>
                                                                <td>AOP Domestik</td>
                                                            <?php else : ?>
                                                                <td><?= $v['name_cust_receh']; ?></td>
                                                            <?php endif ?>
                                                            <td><?= $v['name_item_receh']; ?></td>
                                                            <td><?= $v['no_rfq_receh']; ?></td>
                                                            <td><?= $v['no_wo_receh']; ?></td>
                                                            <td><?= $v['no_sdf_receh']; ?></td>
                                                            <td><?= $v['qty_receh']; ?></td>
                                                            <td><?= $v['lot_del_receh']; ?></td>
                                                            <td><?= $v['locations_receh']; ?>.<?= $v['sub_locations_receh']; ?></td>
                                                        </tr>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->
<?= $this->endSection(); ?>