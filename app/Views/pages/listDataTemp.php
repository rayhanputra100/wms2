<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = []; //array penampung nama supplier yang sudah unik
//melakukan iterasi terhadap hasil fetch data dari supplier
foreach ($item as $v) {
    $rack = $v['rack'];
    //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
    } else {
        continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
    }
    // $count = count($uniqueRack); //untuk menghitung total Rack
}


?>

<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">List Data Warehouse</h4>
                                <button class="btn btn-success btn-sm export-btn my-1"><i class="fas fa-file-excel"></i> Download</button>
                                <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>BPID</th>
                                            <th>No RFQ</th>
                                            <th>No Wo</th>
                                            <th>QTY</th>
                                            <th>Rack</th>
                                            <th>Location</th>
                                            <!-- <th>QR</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php $i = 1; ?>
                                        <?php foreach ($item as $v) : ?>
                                            <?php if (!empty($v['no_rfq'] && $v['no_wo'])) : ?>
                                                <tr>
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $v['bpid']; ?></td>
                                                    <td><?= $v['no_rfq']; ?></td>
                                                    <td><?= $v['no_wo']; ?></td>
                                                    <td><?= $v['qty']; ?></td>
                                                    <td><?= $v['rack']; ?></td>
                                                    <td><?= $v['locations']; ?></td>
                                                    <!-- <td><img src="../assets/images/qrcode/listdata/item-<?= $v["id"]; ?>.png" alt="qritem-<?= $v["id"]; ?>" class="img-fluid w-25"></td> -->
                                                    <td>
                                                        <!-- <a class="btn btn-xs btn-success me-1" href="/gqr/<?= $v['id']; ?>">Generate QR</a> -->
                                                        <a class="btn btn-xs btn-warning me-1" href="/listdata/<?= $v['id']; ?>">Edit</a>
                                                        <a class="btn btn-xs btn-warning me-1" href="/listreset/<?= $v['id']; ?>">Reset</a>
                                                        <form action="/admin/<?= $v['id']; ?>" method="post" class="d-inline">
                                                            <?= csrf_field(); ?>
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Apakah anda yakin ?')">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->


            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<?= $this->endSection(); ?>