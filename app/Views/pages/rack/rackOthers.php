<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = [];
$total = [];

foreach ($item as $v) {
    $rack = $v['rack'];
    //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
    } else {
        continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
    }
    // $count = count($uniqueRack); //untuk menghitung total Rack
}

?>

<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title-box">
                            <h4 class="page-title">List Rack Items</h4>
                            <?php if (session()->getFlashdata('pesan')) : ?>
                                <div class="alert alert-success" role="alert">
                                    <?= session()->getFlashdata('pesan'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="text-center">Rack Others</h4>
                                <?php foreach ($uniqueRack as $i) : ?>
                                    <?php if ($i === "Others") : ?>
                                        <div class="responsive-table-plugin mt-3">
                                            <div class="table-rep-plugin">
                                                <div class="table-responsive" data-pattern="priority-columns">
                                                    <table id="tech-companies-1" class="table table-striped">
                                                        <thead>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 7</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 7") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>

                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 6</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 6") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>

                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 5</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 5") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>
                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 4</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 4") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>

                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 3</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 3") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>

                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 2</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 2") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>
                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th>Rack <?= $i; ?> Area Transit 1</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Area Transit 1") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>

                                                            <tr>
                                                                <?php if ($i === "Others") : ?>
                                                                    <th><?= $i; ?> Gedung D</th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $i) : ?>
                                                                            <?php if ($v['locations'] == $v['rack'] . " Gedung D") : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>

                                                            </tr>




                                                        </tbody>
                                                    </table>
                                                </div> <!-- end .table-responsive -->

                                            </div> <!-- end .table-rep-plugin-->
                                        </div> <!-- end .responsive-table-plugin-->
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div>
            <!-- end row-->


        </div> <!-- container -->

    </div> <!-- content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


</div>
<!-- END wrapper -->


<?= $this->endSection(); ?>