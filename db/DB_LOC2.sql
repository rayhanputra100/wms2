USE [DB_LOC2]
GO
/****** Object:  Table [dbo].[TBackup]    Script Date: 5/24/2023 9:17:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBackup](
	[id] [int] NULL,
	[no_rfq] [varchar](50) NULL,
	[no_wo] [varchar](50) NULL,
	[name_cust] [varchar](50) NULL,
	[qty] [varchar](50) NULL,
	[code_qr] [varchar](50) NULL,
	[locations] [varchar](50) NULL,
	[sub_locations] [varchar](50) NULL,
	[rack] [varchar](50) NULL,
	[warehouse] [varchar](50) NULL,
	[no_tag] [int] NULL,
	[name_item] [varchar](50) NULL,
	[desc_pn] [varchar](50) NULL,
	[bpid] [varchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TData]    Script Date: 5/24/2023 9:17:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TData](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no_rfq] [varchar](50) NULL,
	[no_wo] [varchar](50) NULL,
	[name_cust] [varchar](50) NULL,
	[qty] [int] NULL,
	[code_qr] [varchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[warehouse] [varchar](50) NULL,
	[no_tag] [int] NULL,
	[name_item] [varchar](50) NULL,
	[desc_pn] [varchar](50) NULL,
	[bpid] [varchar](50) NULL,
 CONSTRAINT [PK_TData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TItems]    Script Date: 5/24/2023 9:17:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TItems](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no_rfq] [varchar](50) NULL,
	[no_wo] [varchar](50) NULL,
	[name_cust] [varchar](50) NULL,
	[qty] [varchar](50) NULL,
	[code_qr] [varchar](50) NULL,
	[locations] [varchar](50) NULL,
	[sub_locations] [int] NULL,
	[rack] [varchar](50) NULL,
	[warehouse] [varchar](50) NULL,
	[no_tag] [int] NULL,
	[name_item] [varchar](50) NULL,
	[desc_pn] [varchar](50) NULL,
	[bpid] [varchar](50) NULL,
	[no_sdf] [varchar](50) NULL,
	[lot_del] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[name_item_receh] [varchar](50) NULL,
 CONSTRAINT [PK_Titems] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TOut]    Script Date: 5/24/2023 9:17:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TOut](
	[id_out] [int] IDENTITY(1,1) NOT NULL,
	[id_data] [int] NULL,
	[id_receh] [int] NULL,
	[no_rfq] [varchar](50) NULL,
	[no_wo] [varchar](50) NULL,
	[name_cust] [varchar](50) NULL,
	[name_item] [varchar](50) NULL,
	[qty] [int] NULL,
	[no_tag] [int] NULL,
	[desc_pn] [varchar](50) NULL,
	[bpid] [varchar](50) NULL,
	[warehouse] [varchar](50) NULL,
	[code_qr] [varchar](50) NULL,
	[no_sdf] [varchar](50) NULL,
	[lot_del] [varchar](50) NULL,
	[rack] [varchar](50) NULL,
	[locations] [varchar](50) NULL,
	[sub_locations] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK_TOut] PRIMARY KEY CLUSTERED 
(
	[id_out] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TReceh]    Script Date: 5/24/2023 9:17:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TReceh](
	[id_receh] [int] IDENTITY(1,1) NOT NULL,
	[id_data] [int] NULL,
	[no_rfq_receh] [varchar](50) NULL,
	[no_wo_receh] [varchar](50) NULL,
	[name_cust_receh] [varchar](50) NULL,
	[qty_receh] [int] NULL,
	[code_qr_receh] [varchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[warehouse_receh] [varchar](50) NULL,
	[no_tag_receh] [int] NULL,
	[name_item_receh] [varchar](50) NULL,
	[desc_pn_receh] [varchar](50) NULL,
	[bpid_receh] [varchar](50) NULL,
	[locations_receh] [varchar](50) NULL,
	[sub_locations_receh] [int] NULL,
	[rack_receh] [varchar](50) NULL,
	[no_sdf_receh] [varchar](50) NULL,
	[lot_del_receh] [int] NULL,
 CONSTRAINT [PK_TReceh] PRIMARY KEY CLUSTERED 
(
	[id_receh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TItems] ADD  CONSTRAINT [DF_TItems_qty]  DEFAULT ((0)) FOR [qty]
GO
ALTER TABLE [dbo].[TReceh]  WITH CHECK ADD  CONSTRAINT [FK__TReceh__id_Data__43D61337] FOREIGN KEY([id_data])
REFERENCES [dbo].[TItems] ([id])
GO
ALTER TABLE [dbo].[TReceh] CHECK CONSTRAINT [FK__TReceh__id_Data__43D61337]
GO
